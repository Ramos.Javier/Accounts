package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.CuentaDTO;
import persistencia.dao.interfaz.CuentaDAO;
import utils.EncriptadorAES;
import persistencia.conexion.Conexion;

public class CuentaDAOMYSQL implements CuentaDAO
{
	
	private static CuentaDAOMYSQL instance;
	private static final String insert = "INSERT INTO cuenta (idAccount, app, description, url, user, pass) VALUES(?, ?, ?, ?, ?, ?)";
	private static final String update = "UPDATE cuenta SET app=?, description=?, url=?, user=?, pass=? WHERE idAccount=?";
	private static final String delete = "DELETE FROM cuenta WHERE idAccount=?";
	private static final String readAll = "SELECT * FROM cuenta";
	private static final String readForId = "SELECT * FROM cuenta WHERE idAccount=?";

	public EncriptadorAES encriptador = EncriptadorAES.getInstance();
	
	public static CuentaDAOMYSQL getInstance()
	{
		if (instance == null)
			instance = new CuentaDAOMYSQL();
		return instance;
	}
	
	@Override
	public boolean insert(CuentaDTO cuenta) 
	{
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{	
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setString( 1, cuenta.getIdAccount() );
			statement.setString( 2, encriptador.encriptar(cuenta.getApp()) );
			statement.setString( 3, encriptador.encriptar(cuenta.getDescription()) );
			statement.setString( 4, encriptador.encriptar(cuenta.getUrl()) );
			statement.setString( 5, encriptador.encriptar(cuenta.getUser()) );
			statement.setString( 6, encriptador.encriptar(cuenta.getPass()) );
			
			if(statement.executeUpdate() > 0) { return true; }
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean update(CuentaDTO cuenta) 
	{
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setString( 1, encriptador.encriptar(cuenta.getApp()) );
			statement.setString( 2, encriptador.encriptar(cuenta.getDescription()) );
			statement.setString( 3, encriptador.encriptar(cuenta.getUrl()) );
			statement.setString( 4, encriptador.encriptar(cuenta.getUser()) );
			statement.setString( 5, encriptador.encriptar(cuenta.getPass()) );
			statement.setString( 6, cuenta.getIdAccount() );
			
			if(statement.executeUpdate() > 0)
				return true;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean delete(CuentaDTO cuenta) 
	{
		PreparedStatement statement;
		int chequeoUpdate = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(delete);
			statement.setString(1, cuenta.getIdAccount());
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) 
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<CuentaDTO> readAll() 
	{
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<CuentaDTO> cuentas = new ArrayList<CuentaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readAll);
			resultSet = statement.executeQuery();			
			while(resultSet.next())
			{
				CuentaDTO newCuenta = new CuentaDTO();
				newCuenta.setIdAccount(resultSet.getString("idAccount"));
				
				newCuenta.setApp( encriptador.desencriptar(resultSet.getString("app")) );
				newCuenta.setDescription( encriptador.desencriptar(resultSet.getString("description")) );
				newCuenta.setUrl( encriptador.desencriptar(resultSet.getString("url")) );
				newCuenta.setUser( encriptador.desencriptar(resultSet.getString("user")) );
				newCuenta.setPass( encriptador.desencriptar(resultSet.getString("pass")) );
				
				cuentas.add(newCuenta);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return cuentas;
	}

	@Override
	public CuentaDTO readForId(String idAccount) 
	{
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		CuentaDTO cuenta = new CuentaDTO(); 
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readForId);
			statement.setString(1, idAccount);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				cuenta.setIdAccount(resultSet.getString("idAccount"));
				
				cuenta.setApp( encriptador.desencriptar( resultSet.getString("app")) );
				cuenta.setDescription( encriptador.desencriptar( resultSet.getString("description")) );
				cuenta.setUrl( encriptador.desencriptar( resultSet.getString("url")) );
				cuenta.setUser( encriptador.desencriptar( resultSet.getString("user")) );
				cuenta.setPass( encriptador.desencriptar( resultSet.getString("pass")) );	
			}	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return cuenta;
	}
	
}