package persistencia.dao.interfaz;

import dto.AutenticacionDTO;

public interface AutenticacionDAO 
{

	public boolean update (AutenticacionDTO authen);
	
	public AutenticacionDTO readForId (int idHash);

}