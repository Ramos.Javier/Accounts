package persistencia.dao.interfaz;

import java.util.List;

import dto.CuentaDTO;

public interface CuentaDAO 
{

	public boolean insert (CuentaDTO cuenta);

	public boolean update (CuentaDTO cuenta);
	
	public boolean delete (CuentaDTO cuenta);
	
	public List<CuentaDTO> readAll ();
	
	public CuentaDTO readForId (String idAccouts);

}