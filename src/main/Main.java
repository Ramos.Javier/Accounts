package main;

import java.io.IOException;
import java.net.ServerSocket;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main  extends Application 
{

	public static ServerSocket SERVER_SOCKET;

	@Override
	public void start(Stage primaryStage) 
	{
		try 
		{	
			SERVER_SOCKET = new ServerSocket(20241); // a�o+app = 20241

			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("/presentacion/vista/VentanaLogin.fxml"));
			
			Pane ventana = (Pane) loader.load();
			
			Scene scene = new Scene(ventana, 250, 250);
			primaryStage.setScene(scene);
			
			primaryStage.getIcons().add(new Image("/resources/images/accounts.png"));
			primaryStage.setTitle("Accounts");
			
			primaryStage.setResizable(false);
			primaryStage.show();
			
			// Cerramos el socket al cerrar la ventana login.
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			    @Override
			    public void handle(WindowEvent t) 
			    {
			    	try 
			    	{
						SERVER_SOCKET.close();
						System.exit(0);
					} 
			    	catch (IOException e) 
			    	{
			    		System.out.println( e.getMessage() );
					}
			    }
			});
			
	        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
	        primaryStage.setX((screenBounds.getWidth() - primaryStage.getWidth()) / 2);
	        primaryStage.setY((screenBounds.getHeight() - primaryStage.getHeight()) / 2);
		} 
		catch(Exception e) 
		{  
    		System.out.println( e.getMessage() );
		}
	}
	
	public static void main(String[] args) 
	{
		launch(args);
	}

}