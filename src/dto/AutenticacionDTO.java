package dto;

public class AutenticacionDTO 
{

	private int idHash;
	private String hash;

	public int getIdHash() 
	{
		return idHash;
	}

	public void setIdHash(int idHash) 
	{
		this.idHash = idHash;
	}

	public String getHash() 
	{
		return hash;
	}

	public void setHash(String hash) 
	{
		this.hash = hash;
	}
	
}