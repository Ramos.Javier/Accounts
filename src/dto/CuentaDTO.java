package dto;

public class CuentaDTO 
{

	private String idAccount;
	private String app;
	private String description;
	private String url;
	private String user;
	private String pass;
	
	public String getIdAccount() 
	{
		return idAccount;
	}

	public void setIdAccount(String idAccount) 
	{
		this.idAccount = idAccount;
	}

	public String getApp() 
	{
		return app;
	}

	public void setApp(String app) 
	{
		this.app = app;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description) 
	{
		this.description = description;
	}

	public String getUrl() 
	{
		return url;
	}

	public void setUrl(String url) 
	{
		this.url = url;
	}

	public String getUser() 
	{
		return user;
	}

	public void setUser(String user) 
	{
		this.user = user;
	}

	public String getPass() 
	{
		return pass;
	}

	public void setPass(String pass) 
	{
		this.pass = pass;
	}
	
}