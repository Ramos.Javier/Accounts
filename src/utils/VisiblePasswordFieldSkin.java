package utils;

import com.sun.javafx.scene.control.skin.TextFieldSkin;

import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;

public class VisiblePasswordFieldSkin extends TextFieldSkin {

	private boolean mask = true;
	
	public VisiblePasswordFieldSkin(PasswordField textField, Button btnShow) {
	    super(textField);
	    
	    btnShow.setOnMouseClicked(event -> {
	        if(mask) mask = false;
	        else mask = true;
	        
	        textField.setText(textField.getText());
	        textField.requestFocus();
	        textField.end();
	    });
	    
	    
	    btnShow.setOnKeyReleased(event -> {
	        if(mask) mask = false;
	        else mask = true;
	        
	        textField.setText(textField.getText());
	        textField.requestFocus();
	        textField.end();		    	
	    });
	}
	
	@Override
	protected String maskText(String txt) {
	    if (getSkinnable() instanceof PasswordField && mask) {
	        int n = txt.length();
	        StringBuilder passwordBuilder = new StringBuilder(n);
	        for (int i = 0; i < n; i++) { passwordBuilder.append(BULLET); }
	        return passwordBuilder.toString();
	    } else { return txt; }
	}
	
}