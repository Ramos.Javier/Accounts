package utils;

import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import presentacion.controlador.ControladorVentanaLogin;

public class NotificationLogin extends Thread {

	private ControladorVentanaLogin contro;
	private String type;
	private String msg;
	
	public NotificationLogin(ControladorVentanaLogin contro, String type, String msg){

		this.contro = contro;
		this.type = type;
		this.msg = msg;
		
		// Estilizar Panel de Notificacion.
		if(this.type.equals("error")){
			this.contro.hboxNotification.setStyle("-fx-border-radius: 5; -fx-background-color: #f5b7b1; -fx-background-radius: 8; -fx-border-color:  transparent;");
			Image img0 = new Image("resources/images/error.png");
		    ImageView view0 = new ImageView(img0);
		    view0.setFitHeight(20);
		    view0.setFitWidth(20);
		    contro.lblNoticationImage.setGraphic(view0);
		    contro.lblNoticationMessage.setText(this.msg);
		}
	}
	
	@Override
	public void run() {
		this.deshabilitar();
		
		this.contro.hboxNotification.setVisible(true);
		this.contro.hboxNotification.setOpacity(1);
		this.esperar(2000);
		this.contro.hboxNotification.setOpacity(0.9);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.8);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.7);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.6);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.5);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.4);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.3);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.2);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0.1);
		this.esperar(100);
		this.contro.hboxNotification.setOpacity(0);
		this.contro.hboxNotification.setVisible(false);
		
		this.habilitar();
	}

	private void esperar(int milisegundos)
	{
		try { Thread.sleep(milisegundos); }
		catch ( InterruptedException ex ) { Thread.currentThread().interrupt(); }
	}
	
	public void deshabilitar(){
		this.contro.passAuthentication.setText("");
		
		this.contro.lblFavicon.setDisable(true);
		this.contro.btnShow.setDisable(true);
		this.contro.passAuthentication.setDisable(true);
	}
	
	public void habilitar(){
		this.contro.lblFavicon.setDisable(false);
		this.contro.btnShow.setDisable(false);
		this.contro.passAuthentication.setDisable(false);

		Platform.runLater(()->{ this.contro.passAuthentication.requestFocus(); });
	}
	
}