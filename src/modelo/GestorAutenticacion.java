package modelo;

import dto.AutenticacionDTO;

import persistencia.dao.mysql.AutenticacionDAOMYSQL;

public class GestorAutenticacion 
{
	
	private static GestorAutenticacion instance;
	private AutenticacionDAOMYSQL autenticacionDaoSQL;
	
	private GestorAutenticacion() 
	{
		this.autenticacionDaoSQL = AutenticacionDAOMYSQL.getInstance();
	}

	public static GestorAutenticacion getInstance() 
	{
		if ( instance == null )
			instance = new GestorAutenticacion();
		return instance;
	}

	public void update(AutenticacionDTO authen)
	{
		this.autenticacionDaoSQL.update(authen);
	}

	public AutenticacionDTO readForId(int idHash)
	{
		return this.autenticacionDaoSQL.readForId(idHash);
	}
	
}