package presentacion.controlador;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.UUID;

import dto.CuentaDTO;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import modelo.GestorCuentas;
import utils.NotificationsPrincipal;
import utils.PasswordGenerator;

public class ControladorVentanaAgregarEditarCuenta implements ClipboardOwner 
{

	@FXML
	private VBox panelAgregarEditar;

	@FXML
	private TextField txtAplicacion;
	@FXML
	private TextArea txtDescripcion;
	@FXML
	private TextField txtUrl;
	@FXML
	private TextField txtUsuario;
	@FXML
	private TextField txtContrasenia;

	@FXML
	private Label lblCerrar;
	
	@FXML
	private Label lblAplicacion;
	@FXML
	private Label lblDescripcion;
	@FXML
	private Label lblUrl;
	@FXML
	private Label lblUsuario;
	@FXML
	private Label lblContrasenia;	
	
	@FXML
	private Button btnGuardar;

	public CuentaDTO cuenta;
	public String tipo;
	
	public ControladorVentanaPrincipal contro;
	
	public Tooltip tooltip = new Tooltip();
	
	public void initialize(String tipo, CuentaDTO cuenta, ControladorVentanaPrincipal contro)
	{	
		this.tipo = tipo;
		this.cuenta = cuenta;
		this.contro = contro;
		
		this.contro.deshabilitarPantallaPrincipal();
		
	    this.txtDescripcion.getStylesheets().add("styles/formularioAgregarEditar.css");
		
		if (tipo.equals("agregar"))
		{ 			
			this.panelAgregarEditar.setStyle("-fx-background-image: url('resources/images/fondoAgregar.jpg'); -fx-background-size: cover; ");
		}
		else if (tipo.equals("editar"))
		{ 
			this.panelAgregarEditar.setStyle("-fx-background-image: url('resources/images/fondoEditar.jpg'); -fx-background-size: cover; ");
			
			this.txtAplicacion.setText(this.cuenta.getApp());
			this.txtDescripcion.setText(this.cuenta.getDescription());
			this.txtUrl.setText(this.cuenta.getUrl());
			this.txtUsuario.setText(this.cuenta.getUser());
			this.txtContrasenia.setText(this.cuenta.getPass());
		}

		Image img0 = new Image("resources/images/cerrar.png");
	    ImageView view0 = new ImageView(img0);
	    view0.setFitHeight(20);
	    view0.setFitWidth(20);
	    this.lblCerrar.setGraphic(view0);
		
		Image img1 = new Image("resources/images/aplicacion.png");
	    ImageView view1 = new ImageView(img1);
	    view1.setFitHeight(20);
	    view1.setFitWidth(20);
	    this.lblAplicacion.setGraphic(view1);
		
		Image img2 = new Image("resources/images/descripcion.png");
	    ImageView view2 = new ImageView(img2);
	    view2.setFitHeight(20);
	    view2.setFitWidth(20);
	    this.lblDescripcion.setGraphic(view2);
	    
	    Image img3 = new Image("resources/images/url.png");
	    ImageView view3 = new ImageView(img3);
	    view3.setFitHeight(20);
	    view3.setFitWidth(20);
	    this.lblUrl.setGraphic(view3);
	    
		Image img4 = new Image("resources/images/usuario.png");
	    ImageView view4 = new ImageView(img4);
	    view4.setFitHeight(20);
	    view4.setFitWidth(20);
	    this.lblUsuario.setGraphic(view4);
	    
	    Image img5 = new Image("resources/images/contrasenia.png");
	    ImageView view5 = new ImageView(img5);
	    view5.setFitHeight(20);
	    view5.setFitWidth(20);
	    this.lblContrasenia.setGraphic(view5);

	    this.tooltip.setStyle("-fx-text-fill: #FF9A00;");
	    
	    this.btnGuardar.getStylesheets().add("styles/formularioAgregarEditar.css");

	    Platform.runLater( () -> this.lblCerrar.requestFocus() );
	}	

	@FXML
	public void guardarEnter(KeyEvent event) 
	{
		if (event.getCode() == KeyCode.ENTER)
			this.guardarClick(null);
	}
	
	@FXML
	public void guardarClick(ActionEvent event) 
	{
		if ( this.tipo.equals("agregar") )
		{
			CuentaDTO newAccount = new CuentaDTO();
			newAccount.setIdAccount(UUID.randomUUID().toString());
			newAccount.setApp( this.txtAplicacion.getText() );
			newAccount.setDescription( this.txtDescripcion.getText() );
			newAccount.setUrl( this.txtUrl.getText() );
			newAccount.setUser( this.txtUsuario.getText() );
			newAccount.setPass( this.txtContrasenia.getText() );
			GestorCuentas.getInstance().insert(newAccount);
			
			this.contro.borderPaneVentanaPrincipal.setLeft( null );
			
			NotificationsPrincipal on = new NotificationsPrincipal(this.contro, "agregado", "Agregado");
			on.start();
		}
		else if ( this.tipo.equals("editar") )
		{
			this.cuenta.setApp( this.txtAplicacion.getText() );
			this.cuenta.setDescription( this.txtDescripcion.getText() );
			this.cuenta.setUrl( this.txtUrl.getText() );
			this.cuenta.setUser( this.txtUsuario.getText() );
			this.cuenta.setPass( this.txtContrasenia.getText() );
			GestorCuentas.getInstance().update(cuenta);

			this.contro.borderPaneVentanaPrincipal.setLeft( null );
			
			NotificationsPrincipal on = new NotificationsPrincipal(this.contro, "editado", "Editado");
			on.start();
		}
	}

	@FXML
	public void cerrarEnter(KeyEvent event) 
	{
		if (event.getCode() == KeyCode.ENTER) 
			this.cerrarClick(null);
	}

	@FXML
	public void cerrarClick(MouseEvent event)
	{
		this.contro.borderPaneVentanaPrincipal.setLeft( null );
		
	    this.contro.hBoxBuscador.setStyle("-fx-border-radius: 5; -fx-border-width: 2; -fx-border-style:solid; -fx-border-color: #004d67; -fx-background-color: black; -fx-background-radius: 8");
	    this.contro.hBoxActions.setStyle("-fx-border-radius: 5; -fx-border-width: 2; -fx-border-style:solid; -fx-border-color: #4d5656; -fx-background-color: black; -fx-background-radius: 8");
		
		this.contro.btnAgregar.setDisable(false);
		this.contro.MenuButtonBD.setDisable(false);
		this.contro.btnDescargar.setDisable(false);
		this.contro.btnConfigurar.setDisable(false);
		this.contro.btnBuscador.setDisable(false);
		
		this.contro.txtFiltro.setDisable(false);
		this.contro.txtFiltro.setText("");
		this.contro.txtFiltro.requestFocus();
		
		this.contro.filtrar(null);
		this.contro.tblCuentas.setDisable(false);
	}

	@FXML
	public void tooltipAplicacion(MouseEvent event)
	{
		this.tooltip.setText("Limpiar aplicación");
		ImageView view = new ImageView("resources/images/limpiar.png");
		view.setFitHeight(20);
	    view.setFitWidth(20);
		this.tooltip.setGraphic(view);
		this.lblAplicacion.setTooltip(tooltip);
	}
	
	@FXML
	public void tooltipDescripcion(MouseEvent event)
	{
		this.tooltip.setText("Limpiar descripción");
		ImageView view = new ImageView("resources/images/limpiar.png");
		view.setFitHeight(20);
	    view.setFitWidth(20);
		this.tooltip.setGraphic(view);
		this.lblDescripcion.setTooltip(tooltip);
	}
	
	@FXML
	public void tooltipUrl(MouseEvent event)
	{
		this.tooltip.setText("Limpiar url");
		ImageView view = new ImageView("resources/images/limpiar.png");
		view.setFitHeight(20);
	    view.setFitWidth(20);
		this.tooltip.setGraphic(view);
		this.lblUrl.setTooltip(tooltip);
	}
	
	@FXML
	public void tooltipUsuario(MouseEvent event)
	{
		this.tooltip.setText("Limpiar usuario");
		ImageView view = new ImageView("resources/images/limpiar.png");
		view.setFitHeight(20);
	    view.setFitWidth(20);
		this.tooltip.setGraphic(view);
		this.lblUsuario.setTooltip(tooltip);
	}
	
	@FXML
	public void tooltipContrasenia(MouseEvent event)
	{
		this.tooltip.setText("Autogenerar contraseña");
		ImageView view = new ImageView("resources/images/autogenerar.png");
		view.setFitHeight(20);
	    view.setFitWidth(20);
		this.tooltip.setGraphic(view);
		this.lblContrasenia.setTooltip(tooltip);
	}
	
	@FXML
	public void limpiarAplicacion(MouseEvent event)
	{
		this.txtAplicacion.clear();
	    this.txtAplicacion.requestFocus();
	}
	
	@FXML
	public void limpiarDescripcion(MouseEvent event)
	{
		this.txtDescripcion.clear();
	    this.txtDescripcion.requestFocus();
	}
	
	@FXML
	public void limpiarUrl(MouseEvent event)
	{
		this.txtUrl.clear();
	    this.txtUrl.requestFocus();
	}
	
	@FXML
	public void limpiarUsuario(MouseEvent event)
	{
		this.txtUsuario.clear();
	    this.txtUsuario.requestFocus();
	}
	
	@FXML
	public void generatePass(MouseEvent event)
	{
		String passGen = PasswordGenerator.getPassword(15);
	    this.txtContrasenia.requestFocus();
		this.txtContrasenia.setText( passGen );
	    this.txtContrasenia.end();
		this.setClipboard(passGen);
	}
	
	public void setClipboard(String value)
	{
        StringSelection txt = new StringSelection(value);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(txt, this);
	}
	
	@Override
	public void lostOwnership(Clipboard arg0, Transferable arg1) { }
	
}