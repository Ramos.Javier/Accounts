package presentacion.controlador;

import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import dto.AutenticacionDTO;
import dto.CuentaDTO;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import modelo.GestorAutenticacion;
import modelo.GestorCuentas;
import utils.EncriptadorAES;
import utils.NotificationsCambiarPassword;

public class ControladorVentanaCambiarPassword 
{

	@FXML
	private VBox panelCambiarPassword;

	// NOTIFICACION
	@FXML
	public HBox hboxNotification;
	@FXML
	public Label lblNoticationImage;
	@FXML
	public Label lblNoticationMessage;
	
	@FXML
	public Label lblPortada;
	@FXML
	public PasswordField newPass;
	@FXML
	public PasswordField repeatPass;
	
	public EncriptadorAES encriptador;

	public ControladorVentanaPrincipal contro;
	
	public void initialize( ControladorVentanaPrincipal contro )
	{	
		this.contro = contro;
		
		this.encriptador = EncriptadorAES.getInstance();
		
		this.panelCambiarPassword.setStyle("-fx-background-image: url('resources/images/fondoCambiarContrasenia.jpg'); -fx-background-size: cover; ");
		
		Image img1 = new Image("resources/images/clave.png");
	    ImageView view1 = new ImageView(img1);
	    view1.setFitHeight(70);
	    view1.setFitWidth(70);
	    this.lblPortada.setGraphic(view1);
		
	    this.newPass.setStyle("-fx-text-inner-color: #0096C9; -fx-background-color: black;");
	    this.repeatPass.setStyle("-fx-text-inner-color: #0096C9; -fx-background-color: black;");
	    
	    this.lblPortada.requestFocus();

		this.hboxNotification.setVisible(false);
	}	

	@FXML
	public void cambiarContrasenia(KeyEvent event) 
	{
		if (event.getCode().equals(KeyCode.ENTER))
		{
			if (this.newPass.getText().equals(this.repeatPass.getText()))
			{
				//Cambia la encriptacion
				
				//Obetenemos todas las cuentas con la clave anterior.
				List<CuentaDTO> cuentas = GestorCuentas.getInstance().readAll();

				//Antes de actualizar los datos de cuentas, debemos actualizar la key de encriptadorAES.
				encriptador.updateKey( this.newPass.getText() );
				
				for ( CuentaDTO cuenta : cuentas )
				{
					try 
					{
						cuenta.setApp( cuenta.getApp() );
						cuenta.setDescription( cuenta.getDescription() );
						cuenta.setUrl( cuenta.getUrl() );
						cuenta.setUser( cuenta.getUser() );
						cuenta.setPass( cuenta.getPass() );
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
					GestorCuentas.getInstance().update(cuenta);
				}
				
				//Cambia clave de autenticacion
				AutenticacionDTO authen = new AutenticacionDTO();
				authen.setIdHash( 1 );
				authen.setHash( DigestUtils.sha256Hex(this.newPass.getText()) );
				GestorAutenticacion.getInstance().update(authen);
				
				//Actualiza la contraseņa publica statica
				ControladorVentanaPrincipal.contrasenia = this.newPass.getText();
				
				NotificationsCambiarPassword on = new NotificationsCambiarPassword(this, "cambiado", "Contraseņa cambiada"); 
				on.start();
			}
			else
			{
				NotificationsCambiarPassword on = new NotificationsCambiarPassword(this, "error", "Las contraseņas no coinciden"); 
				on.start();
			}
		}	
	}

}