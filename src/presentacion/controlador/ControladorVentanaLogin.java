package presentacion.controlador;

import java.io.IOException;

import org.apache.commons.codec.digest.DigestUtils;

import dto.AutenticacionDTO;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import main.Main;
import modelo.GestorAutenticacion;
import utils.NotificationLogin;
import utils.VisiblePasswordFieldSkin;

public class ControladorVentanaLogin 
{

	@FXML
	public VBox panelLogin;

	
	@FXML
	public Label lblFavicon;
	@FXML
	public Button btnShow;
	@FXML
	public PasswordField passAuthentication;


	public AutenticacionDTO authen;
	
	public boolean show;
	

	// NOTIFICACION
	@FXML
	public HBox hboxNotification;
	@FXML
	public Label lblNoticationImage;
	@FXML
	public Label lblNoticationMessage;
	
	
	public void initialize()
	{	
	    this.show = false;

	    this.btnShow.getStylesheets().add("styles/panelControlPrincipal.css");
	    
		Image img = new Image("resources/images/show.png");
	    ImageView view = new ImageView(img);
	    view.setFitHeight(17);
	    view.setFitWidth(17);
	    this.btnShow.setGraphic(view);
	    
	    
		this.panelLogin.setStyle(" -fx-background-image: url('resources/images/fondoLogin.jpg'); -fx-background-size: cover; ");
		
		this.authen = GestorAutenticacion.getInstance().readForId(1);
		
		Image img1 = new Image("resources/images/huella-dactilar.png");

	    ImageView view1 = new ImageView(img1);
	    view1.setFitHeight(110);
	    view1.setFitWidth(110);
	    this.lblFavicon.setGraphic(view1);
	    
	    this.passAuthentication.setStyle("-fx-text-inner-color:#0096C9; -fx-background-color: black;");
	    this.passAuthentication.setSkin(new VisiblePasswordFieldSkin(this.passAuthentication, this.btnShow));

	    Platform.runLater(() -> this.passAuthentication.requestFocus());

		this.hboxNotification.setVisible(false);
	}
	
	@FXML
	public void showTyped(KeyEvent event) 
	{
		this.showClick(null);
	}
	
	@FXML
	public void showClick(ActionEvent event) 
	{
		if (show) {
			Image img = new Image("resources/images/show.png");
		    ImageView view = new ImageView(img);
		    view.setFitHeight(17);
		    view.setFitWidth(17);
		    this.btnShow.setGraphic(view);
		    this.show = false;
		}
		else {
			Image img = new Image("resources/images/hidden.png");
		    ImageView view = new ImageView(img);
		    view.setFitHeight(17);
		    view.setFitWidth(17);
		    this.btnShow.setGraphic(view);
		    this.show = true;
		}
	}
	
	@FXML
	public void loguearse(KeyEvent event) 
	{
		if (event.getCode().equals(KeyCode.ENTER))
		{
			if ( this.authen.getHash().equals( DigestUtils.sha256Hex(this.passAuthentication.getText()) ) )
			{
				Stage stage = (Stage) this.passAuthentication.getScene().getWindow();
				stage.close();
				
			    this.abrirVentanaPrincipal();
			}
			else
			{
				NotificationLogin on = new NotificationLogin(this, "error", "Contraseņa incorrecta");
				on.start();
			}
		}
	}
	
	private void abrirVentanaPrincipal()
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/presentacion/vista/VentanaPrincipal.fxml"));
		
		try 
		{
			//hacemos una referencia para cargar al padre.
			Parent root = loader.load();
			
			//Scene scene = new Scene(root, 750, 427);
			Scene scene = new Scene(root);
			
			Stage stage = new Stage();
			stage.setTitle("Accounts");
			stage.getIcons().add(new Image("/resources/images/accounts.png"));
			
			ControladorVentanaPrincipal contro = loader.getController();
			contro.initialize(this.passAuthentication.getText());
			
			stage.setScene(scene);
			
			stage.setResizable(true);
			stage.setMinWidth(800);
			stage.setMinHeight(500);
			
			stage.show();
			
			// Cerramos el socket al cerrar la ventana principal.
			stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			    @Override
			    public void handle(WindowEvent t) 
			    {
			    	try 
			    	{
			    		Main.SERVER_SOCKET.close();
						System.exit(0);
					} 
			    	catch (IOException e) 
			    	{
			    		System.out.println( e.getMessage() );
					}
			    }
			});
			
	        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
	        stage.setX((screenBounds.getWidth() - stage.getWidth()) / 2);
	        stage.setY((screenBounds.getHeight() - stage.getHeight()) / 2);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
}